package com.jcastro.aad.juegos;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Clase controlador del MVC en esta clase se deben pedir los campos a la vista para poder
 * pasarlos al modelo para que realice los metodos.
 */
public class Controlador implements ActionListener, TableModelListener, MouseListener {
    private Vista vista;
    private Modelo modelo;

    /*
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscar) {
            if (vista.txtBuscar.getText().isEmpty()) {
                try {
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } else {
                if (vista.txtBuscar.getText().length() >= 1) {
                    String valorBusqueda = vista.txtBuscar.getText();
                    try {
                        cargarFilas(modelo.buscarNombre(valorBusqueda));
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }
            }
        }
    }
    */


    private enum tipoEstado {conectado, desconectado}
    private tipoEstado estado;
    private int id;

    /**
     * Constructor del controlador, aqui inicio los listeneres
     * @param vista objeto de la clase vista
     * @param modelo objeto de la clase modelo
     */
    Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        this.estado = tipoEstado.desconectado;

        modelo.cargarProperties();

        iniciarTabla();
        addActionListener(this);
        addTableModelListener(this);
        addMouseClickListener(this);
    }

    /**
     * Action listeners de los botones
     * @param listener listener
     */
    private void addActionListener(ActionListener listener) {
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemConfiguracion.addActionListener(listener);
        vista.btnOrdenarMayor.addActionListener(listener);
        vista.btnPrecioMenor.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
    }

    /**
     * Action model listener de el dtm de la tabla
     * @param listener listener
     */
    private void addTableModelListener(TableModelListener listener) {
        vista.dtmTabla.addTableModelListener(listener);
    }

    /**
     * Mouse clicker listener para usarlo en la tabla
     * @param listener listener
     */
    private void addMouseClickListener(MouseListener listener) {
        vista.table.addMouseListener(listener);
    }

    /**
     * Metodo para darle los colum indentifiers a la tabla
     */
    private void iniciarTabla() {
        String[] headers = {"id", "titulo", "genero", "fecha_lanzamiento", "precio"} ;
        vista.dtmTabla.setColumnIdentifiers(headers);
    }

    /**
     * Metodo para cargar las filas de la tabla al recibir el result set de MySQL
     * @param resultSet result set
     * @throws SQLException excepcion de sql
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[5];
        vista.dtmTabla.setRowCount(0);
        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);

            vista.dtmTabla.addRow(fila);
        }
        /*if (resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + "CARGADAS");
        }*/
    }

    /**
     * Este es el action performed donde tendre un swtich case que leera los action command de los botones
     * y ejecutara un case u otro dependiendo del action command recibido
     * @param e lectura del action command del boton
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            //  Este case nos sirve para crear nuevos campos en la bbdd
            case "Nuevo" : {
                if (vista.txtTitulo == null || vista.txtGenero == null || vista.dpFechaLanzamiento == null || vista.txtPrecio == null) {
                    System.out.println("UFFF");
                } else {
                    try {
                        modelo.insertarJuegos(vista.txtTitulo.getText(),
                                vista.txtGenero.getText(), vista.dpFechaLanzamiento.getDate(),
                                Double.parseDouble(vista.txtPrecio.getText()));

                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            } break;
            //  Este case nos permite buscar un elemento de la bbdd bucando en concreto por el campo genero
            case "Buscar" : {
                String genero = vista.txtBuscar.getText();

                if (vista.txtBuscar.getText().equals("")) {
                    try {
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        cargarFilas(modelo.buscarNombre(genero));
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            } break;
            //  Case para eliminar un item de la bbdd
            case "Eliminar" : {
                int filaBorrar = vista.table.getSelectedRow();
                int idBorrar = (Integer)vista.dtmTabla.getValueAt(filaBorrar, 0);
                try {
                    modelo.eliminarJuego(idBorrar);
                    vista.dtmTabla.removeRow(filaBorrar);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } break;
            //  Case para la modificacion de un item de la bbdd
            case "Modificar" : {
                try {
                    modelo.modificar(vista.txtTitulo.getText(), vista.txtGenero.getText(), vista.dpFechaLanzamiento.getDate(), vista.txtPrecio.getText(), id);
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } break;
            //  Case para el item menu de salir, que cerrara el programa
            case "Salir" : {
                System.exit(0);
            } break;
            //  Item conectar para el item menu conectar, cambiara a desconectar cuando se pulse y viceversa
            case "Conectar" : {
                if (estado == tipoEstado.desconectado) {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            } break;
            //  Case para el item de menu configuracion que te permite seleccionar la ruta de la conexion mysql
            case "Configuracion" : {
                new DialogoConfiguracionVista();
            } break;
            //  Case para ordenar la tabla de mayor precio al menor
            case "OrdenarMayor" : {
                try {
                    cargarFilas(modelo.ordenarPorPrecioAscendente());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } break;
            //  Case para ordenar la tabla del menor precio al mayor
            case "OrdenarMenor" : {
                try {
                    cargarFilas(modelo.ordenarPorPrecioDescendente());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } break;
        }
    }

    /**
     * Metodo para poder actualizar la tabla desde la propia tabla, dan errores de cast al intentar modificar
     * los campos date o double
     * @param e devolucion de la posicion de la tabla
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            System.out.println("Actualizada");
            int filaModificada = e.getFirstRow();
            try {
                modelo.modificarJuego(
                        (Integer)vista.dtmTabla.getValueAt(filaModificada, 0),
                        (String)vista.dtmTabla.getValueAt(filaModificada,1),
                        (String)vista.dtmTabla.getValueAt(filaModificada, 2),
                        (Date)vista.dtmTabla.getValueAt(filaModificada, 3),
                        (double)vista.dtmTabla.getValueAt(filaModificada, 4));
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Metodo mouseClicker que leera donde realizamos la pulsacion en la tabla, y mostrara los campos en
     * los txt de la vista
     * @param e posicion de la pulsacion
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int fila = vista.table.getSelectedRow();

        id = (Integer) vista.dtmTabla.getValueAt(fila, 0);
        String titulo = (String) vista.dtmTabla.getValueAt(fila, 1);
        String genero = (String) vista.dtmTabla.getValueAt(fila, 2);
        Date fecha = (Date) vista.dtmTabla.getValueAt(fila, 3);
        double precio = (Double) vista.dtmTabla.getValueAt(fila, 4);

        vista.txtTitulo.setText(titulo);
        vista.txtGenero.setText(genero);
        vista.dpFechaLanzamiento.setDate(LocalDate.parse(String.valueOf(fecha)));
        vista.txtPrecio.setText(String.valueOf(precio));
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }


}
