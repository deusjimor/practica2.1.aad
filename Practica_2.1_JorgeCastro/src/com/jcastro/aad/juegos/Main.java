package com.jcastro.aad.juegos;

public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        new Controlador(vista, modelo);
    }

}
