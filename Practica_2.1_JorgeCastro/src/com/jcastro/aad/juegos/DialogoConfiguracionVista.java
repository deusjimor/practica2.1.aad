package com.jcastro.aad.juegos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class DialogoConfiguracionVista extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtHost;
    private JTextField txtPuerto;
    private JTextField txtBbdd;
    private JTextField txtUsuario;
    private JTextField txtPassword;

    public DialogoConfiguracionVista() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setModal(true);

        cargar();

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        setLocationRelativeTo(null);
        setSize(new Dimension(400, 200));
        setResizable(true);
        pack();
        setVisible(true);
    }

    private void onOK() {
        Properties properties = new Properties();

        properties.setProperty("host", txtHost.getText());
        properties.setProperty("puerto", txtPuerto.getText());
        properties.setProperty("bbdd", txtBbdd.getText());
        properties.setProperty("usuario", txtUsuario.getText());
        properties.setProperty("password", txtPassword.getText());

        try {
            properties.store(new FileWriter("configuracion.conf"), "Fichero de configuracion");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        dispose();
    }

    private void cargar() {
        Properties properties = new Properties();

        try {
            properties.load(new FileReader("configuracion.conf"));

            txtHost.setText(properties.getProperty("host"));
            txtPuerto.setText(properties.getProperty("puerto"));
            txtBbdd.setText(properties.getProperty("bbdd"));
            txtUsuario.setText(properties.getProperty("usuario"));
            txtPassword.setText(properties.getProperty("password"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
