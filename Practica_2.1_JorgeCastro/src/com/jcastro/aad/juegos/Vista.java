package com.jcastro.aad.juegos;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;

/**
 * Clase vista, la cual contiene toda la interfaz grafica que puede usar el usurio, devuelve
 * posiciones y los action command que interpretara el controaldor
 */
public class Vista {
    JTextField txtBuscar;
    JTextField txtTitulo;
    JTextField txtGenero;
    JTextField txtPrecio;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    DatePicker dpFechaLanzamiento;
    JTable table;
    private JPanel panel;
    JButton btnOrdenarMayor;
    JButton btnPrecioMenor;
    JButton btnModificar;

    DefaultTableModel dtmTabla;
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JMenuItem itemConfiguracion;
    private JFrame frame;

    /**
     * Constructor de la vista
     */
    Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtmTabla = new DefaultTableModel();
        table.setModel(dtmTabla);

        //table.setDefaultEditor(Object.class, null);
        crearMenu();

        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        txtPrecio.addKeyListener(new KeyAdapter() {
        });
    }

    /**
     * Metodo para crear el menu con las tres opciones
     */
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");

        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        itemConfiguracion = new JMenuItem("Configuracion");
        itemConfiguracion.setActionCommand("Configuracion");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);
        menuArchivo.add(itemConfiguracion);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }

}
