package com.jcastro.aad.juegos;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Clase modelo en la cual tendremos los metodos que pedira la clase controlador y que se ejecutaran sobre la bbdd
 */
class Modelo {
    private Connection conexion;
    static private String host;
    static private String puerto;
    static private String bbdd;
    static private String usuario;
    static private String password;

    /**
     * Metodo para cargar las propiedades del fichero de configuracion
     */
    void cargarProperties() {
        Properties properties = new Properties();

        try {
            properties.load(new FileReader("configuracion.conf"));

            host = properties.getProperty("host");
            puerto = properties.getProperty("puerto");
            bbdd = properties.getProperty("bbdd");
            usuario = properties.getProperty("usuario");
            password = properties.getProperty("password");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo conectar para hacer la conexion con la bbdd
     * @throws SQLException excepcion de SQL
     */
    void conectar() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://" + host + ":" + puerto + "/" + bbdd, usuario, password);
    }

    /**
     * Metodo desconectar para relizar la desconexion de la bbdd
     * @throws SQLException excepcion de SQL
     */
    void desconectar() throws SQLException {
        conexion.close();
        conexion = null;
    }

    /**
     * Metodo que ejecuta una consulta para mostrar todos los datos de la tabla
     * @return representa la respuesta de la consulta de sql
     * @throws SQLException excepcion de SQL
     */
    ResultSet obtenerDatos() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }

        String consulta = "SELECT * FROM juego";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);

        return sentencia.executeQuery();
    }

    /**
     * Metodo que ejecuta una sentencia en la bbdd para introducir un nuevo item en la bbdd
     * @param titulo parametro del titulo
     * @param genero parametro del genero
     * @param fechaEstreno parametro de la fecha
     * @param precio parametro del precio
     * @throws SQLException excepcion de SQL
     */
    void insertarJuegos (String titulo, String genero, LocalDate fechaEstreno, double precio) throws SQLException {
        if (conexion == null) {
            return;
        }
        if (conexion.isClosed()) {
            return;
        }

        String consulta = "INSERT into juego(titulo, genero, fecha_lanzamiento, precio) VALUES (?,?,?,?)";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, titulo);
        sentencia.setString(2, genero);
        sentencia.setDate(3, Date.valueOf(fechaEstreno));
        sentencia.setDouble(4, precio);

        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Metodo que ejecuta una consulta que eliminara el elemento del cual pasamos su id para eliminarlo
     * de la bbdd
     * @param id variable de la id para seleccionar el elemento correspondiente
     * @throws SQLException excepcion de sql
     */
    void eliminarJuego(int id) throws SQLException {
        if (conexion == null) {
            return;
        }
        if (conexion.isClosed()) {
            return;
        }

        String consulta = "DELETE FROM juego WHERE id = ?";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, id);

        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Metodo para modificar un campo de la bbdd desde la tabla
     * @param id variable de la id para identificar el campo a modificar
     * @param titulo variable para el titulo
     * @param genero vairiable para el genero
     * @param fechaLanzamiento variable para la fecha
     * @param precio variable para el precio
     * @throws SQLException excepcion de sql
     */
    void modificarJuego (int id, String titulo, String genero, Date fechaLanzamiento, double precio) throws SQLException {
        if (conexion == null) {
            return;
        }
        if (conexion.isClosed()) {
            return;
        }

        String consulta = "UPDATE juego SET titulo = ?, genero = ?, fecha_lanzamiento = ?, precio = ? WHERE id = ?";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, titulo);
        sentencia.setString(2, genero);
        sentencia.setDate(3, fechaLanzamiento);
        sentencia.setDouble(4, precio);
        sentencia.setInt(5, id);

        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Metodo para modificar los campos de la tabla desde los campos obtenidos desde la vista
     * @param titulo variable para el titulo
     * @param genero variable para el genero
     * @param fecha variable para la fecha
     * @param precio vriable para el precio
     * @param id variable para la id que nos permitira identificar el elemento a modificar
     * @throws SQLException excepcion de sql
     */
    void modificar (String titulo, String genero, LocalDate fecha, String precio, int id) throws SQLException {
        if (conexion == null) {
            return;
        }
        if (conexion.isClosed()) {
            return;
        }

        String consulta = "UPDATE juego SET titulo = ?, genero = ?, fecha_lanzamiento = ?, precio = ? WHERE id = ?";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, titulo);
        sentencia.setString(2, genero);
        sentencia.setDate(3, Date.valueOf(fecha));
        sentencia.setDouble(4, Double.parseDouble(precio));
        sentencia.setInt(5, id);

        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Metodo para ejecutar una consulta para buscar una fila de la base de datos dependiendo del campo de genero
     * @param genero variable genero sera por la cual realizarmos la consulta
     * @return representa la consulta de sql
     * @throws SQLException excepcion de sql
     */
    ResultSet buscarNombre (String genero) throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }

        String consulta = "SELECT * FROM juego WHERE genero like " + "\'" + genero + "\'";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        return sentencia.executeQuery();
    }

    /**
     * Metodo que realiza una consulta para ordenar la tabla por precio
     * @return representa la consulta de sql
     * @throws SQLException excepcion de sql
     */
    ResultSet ordenarPorPrecioAscendente() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }

        String consulta = "SELECT * FROM juego ORDER BY precio DESC";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        return sentencia.executeQuery();
    }

    /**
     * Metodo que realiza una consulta para ordenar la tabla por precio
     * @return representa la consulta de sql
     * @throws SQLException excepcion de sql
     */
    ResultSet ordenarPorPrecioDescendente() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }

        String consulta = "SELECT * FROM juego ORDER BY precio ASC";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        return sentencia.executeQuery();
    }
}
